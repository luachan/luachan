<?php
// This script assumes there is at least one normal (non-priority)
// banner!

// Get the files in a directory, returns null if the directory does
// not exist.
function getFilesInDirectory($dir) {
    if (! is_dir($dir)) {
        return null;
    }

    return array_diff(scandir($dir), array('.', '..'));
}

// Serve a random banner and exit.
function serveRandomBanner($dir, $files) {
    $name = $files[array_rand($files)];
    header("Location: //".$_SERVER["SERVER_NAME"]."/banners/".$name);
    exit;
}

// Get all the banners
$bannerDir = "banners/";
$priorityDir = "banners_priority/";

$banners = getFilesInDirectory($bannerDir);
$priority = getFilesInDirectory($priorityDir);

// If there are priority banners, serve 1/3rd of the time.
if($priority !== null && count($priority) !== 0 && rand(0,2) === 0) {
    serveRandomBanner($priorityDir, $priority);
}

serveRandomBanner($bannerDir, $banners);
?>
