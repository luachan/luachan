<?php
	$theme = Array();
	
	// Theme name
	$theme['name'] = 'Index Luachan';
	// Description (you can use Tinyboard markup here)
	$theme['description'] = 
'Fork do tema Frameset.';
	$theme['version'] = 'v0.1';
	
	// Theme configuration	
	$theme['config'] = Array();
	
	$theme['config'][] = Array(
		'title' => 'Título do site',
		'name' => 'title',
		'type' => 'text'
	);
	
	$theme['config'][] = Array(
		'title' => 'Slogan',
		'name' => 'subtitle',
		'type' => 'text',
		'comment' => '(optional)'
	);
	
	$theme['config'][] = Array(
		'title' => 'Arquivo HTML principal',
		'name' => 'file_main',
		'type' => 'text',
		'default' => $config['file_index'],
		'comment' => '(ex. "index.html")'
	);
	
	$theme['config'][] = Array(
		'title' => 'Sidebar',
		'name' => 'file_sidebar',
		'type' => 'text',
		'default' => 'sidebar.html',
		'comment' => '(eg. "sidebar.html")'
	);
	
	$theme['config'][] = Array(
		'title' => 'Notícias',
		'name' => 'file_news',
		'type' => 'text',
		'default' => 'news.html',
		'comment' => '(eg. "news.html")'
	);
	
	// Unique function name for building everything
	$theme['build_function'] = 'luachan_index_build';
?>
