Luachan - Um fork do lainchan
========================================================
Sobre
------------
Luachan é um fork do [Lainchan](https://github.com/lainchan/lainchan), que é um fork do [vichan](http://github.com/vichan-devel/vichan).

Instalação
-------------
1.	Faça o download do Luachan e o descompacte para sua pasta do servidor web ou pegue a última versão com:

        git clone https://bitbucket.org/luachan/luachan
	
2.	Vá até o ```install.php``` em seu navegador e siga as instruções.

3.	O Luachan deve estar instalado agora. Logue no ```mod.php``` com o usuário e senha padrão: **admin / password**.

4.	Se quiser a configuração idêntica a configuração inicial do luachan, altere as variáveis de conexão a database e os salts do arquivo ```inc/instance-config.lua.php```, e então o renomeie para ```instance-config.php```, ficando ```inc/instance-config.php```. Logo após isso, vá no ```mod.php``` e clique em ```Reconstruir```.
