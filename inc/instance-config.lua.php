<?php
/* cloudflare */
	$_SERVER['REMOTE_ADDR'] = isset($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : $_SERVER['REMOTE_ADDR'];

/* db */

	$config['db']['server'] = 'localhost';
	$config['db']['database'] = 'chan';
	$config['db']['prefix'] = '';
	$config['db']['user'] = 'chan';
	$config['db']['password'] = '12345';

/* cookies/salts */

	$config['cookies']['mod'] = 'mod';
	$config['cookies']['salt'] = 'salt';
	$config['secure_trip_salt'] = 'trip';

/* html */

	$config['page_nav_top'] = true;
	$config['country_flags_condensed'] = false;
// Additional lines added to the footer of all pages.
	$config['footer'][] = _('Luachan.');

/* slogan global */

	$config['global_message'] = 'High tech, low life.';

/* boardlinks */

	$config['boards'] = array(
		array('g', 'mod'),
	 	array('gnu', 'mobi', '0xffff', 'opsec', 'proj', 'placa', 'ricing'),
	 	array('regras' =>  '//'.$_SERVER['HTTP_HOST'].'/regras.html', '$$$' =>  '//'.$_SERVER['HTTP_HOST'].'/doe.html', 'faq' =>  '//'.$_SERVER['HTTP_HOST'].'/faq.html', 'irc' => '//'.$_SERVER['HTTP_HOST'].'/irc.html'),
		array('enter' => 'https://27chan.org/enter/', 'comp' => 'https://55chan.org/comp/'));

/* ban */

// Allow users to appeal bans through Tinyboard.
	$config['ban_appeals'] = true;
	$config['ban_appeals_max'] = 1;

/* spam */

/// Number of hidden fields to generate.
	$config['spam']['hidden_inputs_min'] = 4;
	$config['spam']['hidden_inputs_max'] = 12;

// How many times can a "hash" be used to post?
	$config['spam']['hidden_inputs_max_pass'] = 12;

// How soon after regeneration do hashes expire (in seconds)?
	$config['spam']['hidden_inputs_expire'] = 60 * 60 * 3; // three hours

// Whether to use Unicode characters in hidden input names and values.
	$config['spam']['unicode'] = true;

// Check for proxy
	$config['proxy_check'] = true;
	$config['proxy_save'] = true;

// Spam unblock
	$config['dnsbl_exceptions'][] = '127.0.0.1';

/* posts */

//general vars
	$config['anonymous'] = array('Anônimo');
	$config['show_filename'] = true;
	$config['flood_time'] = 10;
	$config['flood_time_ip'] = 120;
	$config['flood_time_same'] = 30;
	$config['max_body'] = 1800;
	$config['reply_limit'] = 250;
	$config['max_links'] = 20;
	$config['max_filesize'] = 10485760;
	$config['thumb_width'] = 255;
	$config['thumb_height'] = 255;
	$config['max_width'] = 10000;
	$config['max_height'] = 10000;
	$config['threads_per_page'] = 10;
	$config['max_pages'] = 10;
	$config['threads_preview'] = 5;
	$config['root'] = '/';
	$config['image_reject_repost'] = false;
	$config['markup_code'] = "/\[code\](.*?)\[\/code\]/is";

// Do you need a body for your reply posts?
	$config['force_body'] = false;
// Do you need a body for new threads?
	$config['force_body_op'] = true;
// Require an image for threads?
	$config['force_image_op'] = true;

// Strip superfluous new lines at the end of a post.
	$config['strip_superfluous_returns'] = true;

// Track post citations (>>XX). Rebuilds posts after a cited post is deleted, removing broken links.
	// Puts a little more load on the database.
	$config['track_cites'] = true;

// Maximum filename length (will be truncated).
	$config['max_filename_len'] = 255;
// Maximum filename length to display (the rest can be viewed upon mouseover).
	$config['max_filename_display'] = 30;

// Allow users to delete their own posts?
	$config['allow_delete'] = true;
// How long after posting should you have to wait before being able to delete that post? (In seconds.)
	$config['delete_time'] = 10;
	$config['markup_urls'] = true;
	$config['allow_upload_by_url'] = false;
// The timeout for the above, in seconds.
	$config['upload_by_url_timeout'] = 15;
	$config['always_noko'] = true;
	$config['spoiler_images'] = false;

// When true, all names will be set to $config['anonymous'].
	$config['field_disable_name'] = false;
// When true, there will be no email field.
	$config['field_disable_email'] = false;
// When true, there will be no subject field.
	$config['field_disable_subject'] = false;
// When true, there will be no subject field for replies.
	$config['field_disable_reply_subject'] = false;
// When true, a blank password will be used for files (not usable for deletion).
	$config['field_disable_password'] = false;
// When true, users are instead presented a selectbox for email. Contains, blank, noko and sage.
	$config['field_email_selectbox'] = false;

// Which files are allowed?
	$config['allowed_ext'][] = 'jpg';
	$config['allowed_ext'][] = 'jpeg';
	$config['allowed_ext'][] = 'bmp';
	$config['allowed_ext'][] = 'gif';
	$config['allowed_ext'][] = 'png';

	$config['allowed_ext_files'][] = 'mp4';
	$config['allowed_ext_files'][] = 'webm';
	$config['allowed_ext_files'][] = 'txt';
	$config['allowed_ext_files'][] = 'zip';
	$config['allowed_ext_files'][] = 'pdf';
	$config['allowed_ext_files'][] = 'epub';
	$config['allowed_ext_files'][] = 'csv';

// Which icons shall be shown?
	$config['file_icons']['default'] = 'file.png';
	$config['file_icons']['zip'] = 'zip.png';
	$config['file_icons']['webm'] = 'video.png';
	$config['file_icons']['mp4'] = 'video.png';

// Location of above images.
	$config['file_thumb'] = 'static/%s';
// Location of thumbnail to use for spoiler images.
	$config['spoiler_image'] = '/static/spoiler.png';
// Location of thumbnail to use for deleted images.
	$config['image_deleted'] = '/static/deleted.png';

/* php/cache */

	$config['cache']['enabled'] = 'php';

	$config['max_filesize'] = 10 * 1024 * 1024; // 10MB

	$config['report_limit'] = 3;

//$config['timezone'] = 'America/Brasil';

	$config['locale'] = 'pt_BR';

	$config['post_date'] = '%d/%m/%y (%a) %H:%M:%S';
	$config['ban_date'] = '%A %e %B, %Y';

	$config['button_newtopic'] = _('Novo fio');
	$config['button_reply'] = _('Responder');

	$config['genpassword_chars'] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+';

	$config['url_banner'] = '/b.php';
	$config['banner_width'] = 300;
	$config['banner_height'] = 100;

	// How long should the cookies last (in seconds). Defines how long should moderators should remain logged
	// in (0 = browser session).
	$config['cookies']['expire'] = 60 * 60 * 24 * 30 * 6; // ~6 months

	// Whether or not you can access the mod cookie in JavaScript. Most users should not need to change this.
	$config['cookies']['httponly'] = true;

/* filtros */

//$config['wordfilters'][] = array('teste','troca');

/* image settings */
	$config['max_images'] = 3;
	$config['multiimage_method'] = 'split';

	$config['thumb_width'] = 255;
	$config['thumb_height'] = 255;
// Maximum thumbnail dimensions for thread (OP) images.
	$config['thumb_op_width'] = 255;
	$config['thumb_op_height'] = 255;

	$config['thumb_ext'] = 'png';
// Strip EXIF metadata from JPEG files.
	$config['strip_exif'] = true;
	$config['use_exiftool'] = true;

// Which binary shall be used to create thumbs
	$config['thumb_method'] = 'convert';
	$config['gnu_md5'] = '1';

/* css */
	$config['stylesheets']['Dark'] = 'dark.css';
	$config['default_stylesheet'] = array('Dark', $config['stylesheets']['Dark']);

/* js */

	$config['additional_javascript'] = array();

	$config['additional_javascript'][] = 'js/jquery.min.js';
	$config['additional_javascript'][] = 'js/jquery.mixitup.min.js';
	$config['additional_javascript'][] = 'js/jquery-ui.custom.min.js';
	$config['additional_javascript'][] = 'js/auto-reload.js';
	$config['additional_javascript'][] = 'js/options.js';
	$config['additional_javascript'][] = 'js/options/general.js';
	$config['additional_javascript'][] = 'js/style-select.js';
	$config['additional_javascript'][] = 'js/post-hover.js';
	$config['additional_javascript'][] = 'js/favorites.js';
	$config['additional_javascript'][] = 'js/ajax.js';
	$config['additional_javascript'][] = 'js/show-op.js';
	$config['additional_javascript'][] = 'js/smartphone-spoiler.js';
	$config['additional_javascript'][] = 'js/inline-expanding.js';
	$config['additional_javascript'][] = 'js/show-backlinks.js';
	$config['additional_javascript'][] = 'js/webm-settings.js';
	$config['additional_javascript'][] = 'js/expand-video.js';
	$config['additional_javascript'][] = 'js/treeview.js';
	$config['additional_javascript'][] = 'js/expand-too-long.js';
	$config['additional_javascript'][] = 'js/settings.js';
	$config['additional_javascript'][] = 'js/hide-images.js';
	$config['additional_javascript'][] = 'js/expand-all-images.js';
	$config['additional_javascript'][] = 'js/strftime.min.js';
	$config['additional_javascript'][] = 'js/local-time.js';
	$config['additional_javascript'][] = 'js/expand.js';
	$config['additional_javascript'][] = 'js/options/user-css.js';
	$config['additional_javascript'][] = 'js/options/user-js.js';
	$config['additional_javascript'][] = 'js/options/fav.js';
	$config['additional_javascript'][] = 'js/forced-anon.js';
	$config['additional_javascript'][] = 'js/toggle-locked-threads.js';
	$config['additional_javascript'][] = 'js/toggle-images.js';
	$config['additional_javascript'][] = 'js/mobile-style.js';
	$config['additional_javascript'][] = 'js/id_highlighter.js';
	$config['additional_javascript'][] = 'js/id_colors.js';
	$config['additional_javascript'][] = 'js/inline.js';
	$config['additional_javascript'][] = 'js/infinite-scroll.js';
	$config['additional_javascript'][] = 'js/download-original.js';
	$config['additional_javascript'][] = 'js/thread-watcher.js';
	$config['additional_javascript'][] = 'js/quick-reply.js';
	$config['additional_javascript'][] = 'js/quick-post-controls.js';
	$config['additional_javascript'][] = 'js/show-own-posts.js';
	$config['additional_javascript'][] = 'js/youtube.js';
	$config['additional_javascript'][] = 'js/comment-toolbar.js';
	$config['additional_javascript'][] = 'js/catalog-search.js';
	$config['additional_javascript'][] = 'js/thread-stats.js';
	$config['additional_javascript'][] = 'js/quote-selection.js';
	$config['additional_javascript'][] = 'js/post-menu.js';
	$config['additional_javascript'][] = 'js/post-filter.js';
	$config['additional_javascript'][] = 'js/fix-report-delete-submit.js';
	$config['additional_javascript'][] = 'js/image-hover.js';
	$config['additional_javascript'][] = 'js/twemoji/twemoji.js';
	$config['additional_javascript'][] = 'js/file-selector.js';
	$config['additional_javascript'][] = 'js/gallery-view.js';
	$config['additional_javascript'][] = 'js/multi-image.js';
	$config['additional_javascript'][] = 'js/code_tags/run_prettify.js';

	$config['additional_javascript_compile'] = true;

/* video */

	$config['enable_embedding'] = true;
	$config['embedding'] = array(
		array(
			'/^https?:\/\/(\w+\.)?youtube\.com\/watch\?v=([a-zA-Z0-9\-_]{10,11})(&.+)?$/i',
			'<iframe style="float: left;margin: 10px 20px;" width="%%tb_width%%" height="%%tb_height%%" frameborder="0" allowfullscreen="allowfullscreen" id="ytplayer" src="//www.youtube.com/embed/$2"></iframe>'
		),
		array(
			'/^https?:\/\/(\w+\.)?vimeo\.com\/(\d{2,10})(\?.+)?$/i',
			'<object style="float: left;margin: 10px 20px;" width="%%tb_width%%" height="%%tb_height%%"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="//vimeo.com/moogaloop.swf?clip_id=$2&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0" /><embed src="//vimeo.com/moogaloop.swf?clip_id=$2&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="%%tb_width%%" height="%%tb_height%%"></object>'
		));
	$config['embed_width'] = 300;
	$config['embed_height'] = 246;
//webm
	$config['webm']['use_ffmpeg'] = true;
	$config['webm']['allow_audio'] = true;
	$config['webm']['max_length'] = 120;
	$config['webm']['ffmpeg_path'] = 'ffmpeg';
	$config['webm']['ffprobe_path'] = 'ffprobe';



// Error messages
	$config['error']['bot']			= _('Você parece um bot.');
	$config['error']['referer']		= _('Seu browser enviou um HTTP referer inválido.');
	$config['error']['toolong']		= _('O campo %s é muito longo.');
	$config['error']['toolong_body']	= _('Corpo muito longo.');
	$config['error']['tooshort_body']	= _('Corpo muito curto ou vazio.');
	$config['error']['noimage']		= _('Você deve escolher uma imagem.');
	$config['error']['toomanyimages'] 	= _('Você tentou postar muitas imagens!');
	$config['error']['nomove']		= _('Server falhou em concluir seu post.');
	$config['error']['fileext']		= _('Formato não suportado.');
	$config['error']['noboard']		= _('Board inválida!');
	$config['error']['nonexistant']		= _('Thread non ecxiste.');
	$config['error']['locked']		= _('Thread Trancada.');
	$config['error']['reply_hard_limit']	= _('Thread chegou ao limite de postagens.');
	$config['error']['image_hard_limit']	= _('Thread chegou ao limite de imagens.');
	$config['error']['nopost']		= _('Você não postou.');
	$config['error']['flood']		= _('Flood detectado; Post descartado.');
	$config['error']['spam']		= _('Sua solicitação parece automática, descartada.');
	$config['error']['unoriginal']		= _('Conteúdo não original!');
	$config['error']['muted']		= _('Conteúdo não original! Você foi mutado por %d segundos.');
	$config['error']['youaremuted']		= _('Você está mutado! Expira em %d segundos.');
	$config['error']['dnsbl']		= _('Seu IP está listado em %s.');
	$config['error']['toomanylinks']	= _('Muitos links; flood detectado.');
	$config['error']['toomanycites']	= _('Muitas citações; post descartado.');
	$config['error']['toomanycross']	= _('Muita citações a outra board: post descartado.');
	$config['error']['nodelete']		= _('Você não selecionou nada para apagar.');
	$config['error']['noreport']		= _('Você não selecionou nada para reportar.');
	$config['error']['toomanyreports']	= _('Reporte um post por vez.');
	$config['error']['invalidpassword']	= _('Senha incorreta…');
	$config['error']['invalidimg']		= _('Imagem inválida.');
	$config['error']['unknownext']		= _('Extensão inválida.');
	$config['error']['filesize']		= _('Tamanho máximo de arquivo: %maxsz% bytes<br>Seu arquivo tem: %filesz% bytes');
	$config['error']['maxsize']		= _('Arquivo muito grande.');
	$config['error']['genwebmerror']	= _('Ocorreu um erro ao processar seu webm.');
	$config['error']['webmerror'] 		= _('Ocorreu um erro ao processar seu webm.');//Is this error used anywhere ?
	$config['error']['invalidwebm'] 	= _('Webm inválido.');
	$config['error']['webmhasaudio'] 	= _('O webm upado contém um áudio ou outro tipo de stream adicional.');
	$config['error']['webmtoolong'] 	= _('Seu webm é maior que: ' . $config['webm']['max_length'] . ' segundos.');
	$config['error']['fileexists']		= _('O arquivo <a href="%s">já existe.</a>!');
	$config['error']['fileexistsinthread']	= _('O arquivo <a href="%s">já existe</a> nesse fio!');
	$config['error']['delete_too_soon']	= _('Espere %s antes de apagar. Seja homem!');
	$config['error']['mime_exploit']	= _('<script>alert("LAMMER!");</script>');
	$config['error']['invalid_embed']	= _('Não conseguimos entender a url que tentou embutir.');
	$config['error']['captcha']		= _('Captcha do caralho...');

// Moderator errors
	$config['error']['toomanyunban']	= _('Você só pode desbanir %s usuários por vez. Você tentou %u.');
	$config['error']['invalid']		= _('Usuário ou senha inválidos.');
	$config['error']['notamod']		= _('Você não é mod…');
	$config['error']['invalidafter']	= _('Senha ou usuário inválido. Seu usuário foi deletado ou mudou..');
	$config['error']['malformed']		= _('Cookies inválidos/mal informados.');
	$config['error']['missedafield']	= _('Seu browser fodeu a porra toda.');
	$config['error']['required']		= _('O campo %s é obrigatório..');
	$config['error']['invalidfield']	= _('O campo %s é inválido.');
	$config['error']['boardexists']		= _('Já existe a board %s.');
	$config['error']['noaccess']		= _('Você não tem permissão para fazer isso.');
	$config['error']['invalidpost']		= _('Esse post não existe…');
	$config['error']['404']			= _('404, amigo.');
	$config['error']['modexists']		= _('O mod <a href="?/users/%d">já existe.</a>!');
	$config['error']['invalidtheme']	= _('Esse tema não existe!');
	$config['error']['csrf']		= _('Token inválido! Volte e tente novamente.');
	$config['error']['badsyntax']		= _('O PHP disse: ');

/* mod */
	// Mod links (full HTML).
	$config['mod']['link_delete'] = '[D]';
	$config['mod']['link_ban'] = '[B]';
	$config['mod']['link_warning'] = '[A]';
	$config['mod']['link_bandelete'] = '[B&amp;D]';
	$config['mod']['link_deletefile'] = '[F]';
	$config['mod']['link_spoilerimage'] = '[S]';
	$config['mod']['link_deletebyip'] = '[D+]';
	$config['mod']['link_deletebyip_global'] = '[D++]';
	$config['mod']['link_sticky'] = '[Pinar]';
	$config['mod']['link_desticky'] = '[-Pinar]';
	$config['mod']['link_lock'] = '[Trancar]';
	$config['mod']['link_unlock'] = '[-Trancar]';
	$config['mod']['link_bumplock'] = '[Sage]';
	$config['mod']['link_bumpunlock'] = '[-Sage]';
	$config['mod']['link_editpost'] = '[Editar]';
	$config['mod']['link_move'] = '[Mover]';
	$config['mod']['link_merge'] = '[Mesclar]';
	$config['mod']['link_cycle'] = '[Ciclar]';
	$config['mod']['link_uncycle'] = '[-Ciclar]';

	$config['mod']['default_ban_message'] = _('O USUÁRIO FOI BANIDO POR ESSA POSTAGEM');
	$config['mod']['default_warning_message'] = _('O USUÁRIO FOI ALERTADO POR ESSA POSTAGEM');

	// When moving a thread to another board and choosing to keep a "shadow thread", an automated post (with
	// a capcode) will be made, linking to the new location for the thread. "%s" will be replaced with a
	// standard cross-board post citation (>>>/board/xxx)
	$config['mod']['shadow_mesage'] = _('Movido para %s.');
	// Capcode to use when posting the above message.
	$config['mod']['shadow_capcode'] = 'Mod';
